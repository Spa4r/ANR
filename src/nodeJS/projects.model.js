const mongoose = require('mongoose');

const ProjectsSchema = mongoose.Schema({
    Titre: String,
    Etablissement: String,
    Domaines: String,
    Auteurs: String,
    Date: String,
    Description: String,
});

module.exports = mongoose.model('Projects', ProjectsSchema);