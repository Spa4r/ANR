// create the module and name it scotchApp
// also include ngRoute for all our routing needs
var scotchApp = angular.module('scotchApp', ['ngRoute','slickCarousel']);

// configure our routes
scotchApp.config(function($routeProvider) {
    
    $routeProvider
   
    .when('/accueil', {
        templateUrl : 'views/accueil.html',
        controller  : 'accueilController'
    })
    .when('/projets', {
        templateUrl : 'views/projets.html',
        controller  : 'projetsController'
    })
    .otherwise({
        redirectTo : '/accueil'
    });
});
    
// create the controller and inject Angular's $scope
scotchApp.controller('accueilController', function($scope,$http) {
    $scope.slickConfigLoaded = false;
    
    $http.get('http://localhost:8080/projects/limit/6').success(function (data){
        $scope.projets = data;
        $scope.slickConfigLoaded = true;
    });

    $scope.slickCurrentIndex = 0;
    $scope.slickConfig = {
        dots: true,
        autoplay: true,
        initialSlide: 3,
        infinite: true,
        autoplaySpeed: 4000,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            console.log('before change', Math.floor((Math.random() * 10) + 100));
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            $scope.slickCurrentIndex = currentSlide;
            },
            breakpoint: function (event, slick, breakpoint) {
            console.log('breakpoint');
            },
            destroy: function (event, slick) {
            console.log('destroy');
            },
            edge: function (event, slick, direction) {
            console.log('edge');
            },
            reInit: function (event, slick) {
            console.log('re-init');
            },
            init: function (event, slick) {
            console.log('init');
            },
            setPosition: function (evnet, slick) {
            console.log('setPosition');
            },
            swipe: function (event, slick, direction) {
            console.log('swipe');
            }
        }
    };

    //====================================
    // Slick 2
    //====================================
    $scope.slickConfig2 = {
        autoplay: true,
        dots: true,
        infinite: true,
        autoplaySpeed: 4000,
        slidesToShow: 3,
        slidesToScroll: 3,
        method: {}
    };
});
    
scotchApp.controller('projetsController', function($scope,$http) {
    $http.get('http://localhost:8080/projects').success(function (data){
        $scope.projets = data;
    });
    $scope.message = "L’ANR a financé plus de 18000 projets depuis sa création. Consultez les analyses et études d’impact menées par l’Agence.";
});

scotchApp.config(['slickCarouselConfig', function (slickCarouselConfig) {
    slickCarouselConfig.dots = true;
    slickCarouselConfig.autoplay = true;
}])